## React/Redux web development inside Vagrant

This project demonstrates how to setup a javascript development environment that runs on a 
Linux VM by way of Vagrant and VirtualBox. The project is a simple Hello World react/redux 
application that includes mocha unit tests and babel/webpack build steps.

### Initial Setup
* Install VirtualBox 5.x. I am on 5.1.38
* Install Vagrant 2.2.3
* Install WebStorm, IntelliJ or your javascript editor of choice
* Open a command prompt, `cd` to the code directory, and run `vagrant up`
* Install npm packages:, 
  * `vagrant ssh` into the box
  * `cd` to the mapped `code` directory
  * run `yarn install`
* Build and run:
  * run `yarn build` while still ssh'd into the VM
  * run `yarn start` to boot the webpack dev server
  * Hit http://localhost:8080 from your host browser to test the app 
 
  
### yarn commands

* `yarn lint` : run the linter. See `.eslintrc` for config
* `yarn test` : run unit tests
* `yarn verify` : lint + tests
* `yarn build` : babel and webpack
* `yarn start` : start the dev server
  
  
### Details

The root directory of the project is mapped into the `~/code` directory inside the VM.
You can edit source files directly on the host using your editor of choice. Run yarn from 
a `vagrant ssh` session when you are ready to test. 

# Setting up run configurations in WebStorm/IntelliJ

Webstorm and IntelliJ support running commands in a remote node interpreter. This allows you to create run
configurations for executing unit tests, webpacking, and running the tests server. You can run the commands in WebStorm, and they will execute inside vagrant.

* Follow [these instructions](https://www.jetbrains.com/help/webstorm/configuring-remote-node-interpreters.html#ws_node_configure_remote_node_interpreter_vagrant) for creating a remote node interpreter run configuration 
* Because we are using `nvm`, the node executable resides in `/home/vagrant/.nvm/versions/node/v8.15.0/bin/node` instead of `/usr/bin/node`  
* Set the `Node parameters` to the command that you want to run. 
  * unit tests : `node_modules/mocha/bin/mocha`    
  * building : `node_modules/webpack/bin/webpack`
  * linting : `node_modules/eslint/bin/eslint.js src test --format friendly`  (use `friendly` formatter for clickable error messages )

![Run config screenshot](https://bitbucket.org/hhowe29/webvag/downloads/webstorm-run.png)
