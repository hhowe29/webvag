

export const ActionTypes = {
    RESET_COUNT: 'RESET_COUNT',
    INCREMENT_COUNT : 'INCREMENT_COUNT'
};


export function resetCount() {
    return {
        type: ActionTypes.RESET_COUNT
    };
}

export function incrementCount() {
    return {
        type: ActionTypes.INCREMENT_COUNT
    };
}