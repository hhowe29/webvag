import {ActionTypes} from './actions';

export function appReducer(state = {}, action) {
    switch (action.type) {
        case ActionTypes.INCREMENT_COUNT:
            return {
                ...state,
                count: state.count + 1
            };
        case ActionTypes.RESET_COUNT:
            return {
                ...state,
                count: 0
            };
        default:
            return state;
    }
}