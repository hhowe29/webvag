import React from 'react';
import PropTypes from 'prop-types';

export default class Application extends React.Component {
    render() {
        const {
            count,
            onAdd,
            onReset
        } = this.props;

        console.log('rendering application :', count);

        return (
            <div>
                <p>{'Hello world'}</p>
                <p>{`Click count = ${count}`}</p>
                <button onClick={onAdd}>{'Add 1'}</button>
                <button onClick={onReset}>{'Reset count'}</button>
            </div>
        );
    }
}


Application.propTypes = {
    count: PropTypes.number,
    onAdd: PropTypes.funct,
    onReset: PropTypes.funct
};