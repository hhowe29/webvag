import Application from './application';
import React from 'react';
import ReactDOM from 'react-dom';
import {applyMiddleware, createStore} from 'redux';
import thunk from 'redux-thunk';
import {appReducer} from './reducers';
import {connect, Provider} from 'react-redux';
import {resetCount, incrementCount} from "./actions";

const initialState = {};

const store = createStore(appReducer, initialState, applyMiddleware(thunk));
const mapStateToProps = (state) => state;
const mapDispatchToProps = (dispatch) => {
    return {
        onAdd: () => dispatch(incrementCount()),
        onReset: () => dispatch(resetCount()),
    };
};
const ConnectedApplication = connect(mapStateToProps, mapDispatchToProps)(Application);

ReactDOM.render(
    <Provider store={store} >
        <ConnectedApplication />
    </Provider>,
    document.getElementById('content')
);

store.dispatch(resetCount());