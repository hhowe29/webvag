#!/bin/bash -e
echo "Provisioning virtual machine..."

pm='sudo -E apt-get -qq -y'

echo "Updating package database"
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
$pm update

echo "Upgrading existing packages"
# This fixed a problem with vagrant hanging on the provision with VirtualBox 5.1. Not sure if this is still necessary
sudo DEBIAN_FRONTEND=noninteractive apt-get -y -o DPkg::options::="--force-confdef" -o DPkg::options::="--force-confold"  upgrade grub-pc
$pm upgrade

echo "Installing python software properties"
$pm install python-software-properties

echo "Installing base utilities"
$pm install git nano dos2unix tree

echo "Installing python and development libraries"
$pm install python2.7 python-dev python-pip libjpeg-dev zlib1g-dev libxml2 libxslt-dev
sudo -H curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash

echo "Removing unused packages"
$pm autoremove

echo "Upgrading pip"
sudo -H pip install --upgrade pip


echo "Installing nodejs 8 with nvm"
export NVM_DIR="/home/vagrant/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
nvm install lts/carbon
nvm use lts/carbon
$pm install yarn

echo 'export PATH="$(yarn global bin):$PATH"' >> ~/.bashrc
echo "alias yarn='yarn --no-bin-links'" >> ~/.bashrc
echo "alias npml='npm list --depth 0'" >> ~/.bashrc
source ~/.bashrc
yarn global add create-react-app


if [ -n "${HOST_LS_COLORS}" ]; then
  echo "Setting LS_COLORS"
  echo "export LS_COLORS=\"${HOST_LS_COLORS}\"" | tee -a /home/vagrant/.profile
else
  echo "HOST_LS_COLORS was not set"
fi

echo "Provision complete"