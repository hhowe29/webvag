import {appReducer} from '../../src/reducers';
import {resetCount, incrementCount} from '../../src/actions';

import {expect} from 'chai';

describe('Reducer', () => {
    it('should reduce the reset action correctly', () => {
        const action = resetCount();
        const givenState = {
            foo: 29,
            count: 42
        };
        const expected = {
            ...givenState,
            count: 0
        };

        const observed = appReducer(givenState, action);

        expect(observed).to.deep.equal(expected);
    });

    it('should reduce the increment action correctly', () => {
        const action = incrementCount();
        const givenState = {
            foo: 29,
            count: 42
        };
        const expected = {
            ...givenState,
            count: 43
        };

        const observed = appReducer(givenState, action);

        expect(observed).to.deep.equal(expected);
    });
});
