import {ActionTypes,resetCount, incrementCount} from '../../src/actions';

import {expect} from 'chai';

describe('Actions', () => {
    it('should create the reset action correctly', () => {
        const observed = resetCount();

        expect(observed.type).to.equal(ActionTypes.RESET_COUNT);
    });

    it('should create the increment action correctly', () => {
        const observed = incrementCount();

        expect(observed.type).to.equal(ActionTypes.INCREMENT_COUNT);
    });
});
